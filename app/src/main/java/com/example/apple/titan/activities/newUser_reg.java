package com.example.apple.titan.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.apple.titan.R;

public class newUser_reg extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = newUser_reg.this;
    private NestedScrollView nestedScrollView;

    private AppCompatButton appCompatButtonNext;

    EditText date;
    DatePickerDialog datePickerDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user_reg);
        // initiate the date picker and a button
        date = (EditText) findViewById(R.id.selectDate);
        // perform click event on edit text
        date.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(newUser_reg.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                date.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        initViews();
        initListeners();


    }

    private void initViews()
    {

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        appCompatButtonNext = (AppCompatButton) findViewById(R.id.appCompatButtonNext);

    }

    private void initListeners()
    {
        appCompatButtonNext.setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.appCompatButtonNext:
                // Navigate to RegisterActivity
                Intent intentRegister = new Intent(newUser_reg.this, emergency_contact.class);
                startActivity(intentRegister);
                break;
        }
    }

}
