package com.example.apple.titan.activities;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.apple.titan.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;


public class MainActivity extends AppCompatActivity {
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    private String mDeviceName;
    private String mDeviceAddress;
    private RFduinoService rfduinoService;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic writeCharacteristic;
    private BluetoothGattCharacteristic readCharacteristic;
    private LinearLayout dataLayout;
    private Button clearButton;
    private int state;
    private double temp = 0.25;
    private int preData=0, count,peak=0,percentage;
    private int[] z = new int[100000];


    private GraphView mainGraph;
    private LineGraphSeries<DataPoint>  accelorSeries,newAccelorSeries, filterSeries;
    private int i,j,k;
    private Button previous;


    TextView textViewConnectionStatus , noOfLevel, perOfDrowiness,SensorNotConnected;




    public static final String TAG = "MainActivity";


//    private final BroadcastReceiver bluetoothStateReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
//            if (state == BluetoothAdapter.STATE_ON) {
//                upgradeState(STATE_DISCONNECTED);
//            } else if (state == BluetoothAdapter.STATE_OFF) {
//                downgradeState(STATE_BLUETOOTH_OFF);
//            }
//        }
//    };

    private void sendData(byte[] data) {
        if(writeCharacteristic ==null)
            return;

        // TODO: Implement this function
        writeCharacteristic.setValue(data);
        mBluetoothLeService.writeCharacteristic(writeCharacteristic);
    }

    private short shortFromBytes(byte firstByte, byte secondByte) {
        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.put(firstByte);
        bb.put(secondByte);
        return bb.getShort(0);
    }

//    private void addData(byte[] data) {
//        View view = getLayoutInflater().inflate(android.R.layout.simple_list_item_2, dataLayout, false);
//
//        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
//        text1.setText(HexAsciiHelper.bytesToHex(data));
//
//        String ascii = HexAsciiHelper.bytesToAsciiMaybe(data);
//        if (ascii != null) {
//            TextView text2 = (TextView) view.findViewById(android.R.id.text2);
//            text2.setText(ascii);
//        }
//        Log.i(TAG,"in addData");
//
//        dataLayout.addView(
//                view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
////        Bundle b=new Bundle();
////        b.putSerializable("data",text1);
////        Intent i=new Intent(this, graph.class);
////        i.putExtras(b);
//    }


    public void subscribe() {
        if (mGattCharacteristics == null) {
            Log.e(TAG, "Error subscribe null");
            return;
        }

        try {
            // Find out the index opsition
            final BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(3).get(1);
            writeCharacteristic = characteristic;
            final BluetoothGattCharacteristic characteristic2 = mGattCharacteristics.get(3).get(0);
            readCharacteristic = characteristic2;
            mBluetoothLeService.setCharacteristicNotification(readCharacteristic, true);

            Log.d(TAG,"Subscribed!");
        }
        catch (Exception e) {
            Log.d(TAG,"Result: "+e.getMessage());
        }
    }




    private void dataReceived(byte[] data) {
        Log.i(TAG,"in data Receiving");
        Log.i(TAG, "before Data :"+ data);
//        Log.i(TAG,"Data received: "+data.length);
        View view = getLayoutInflater().inflate(android.R.layout.simple_list_item_2, dataLayout, false);

        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
        text1.setText(HexAsciiHelper.bytesToHex(data));
        String ascii = HexAsciiHelper.bytesToAsciiMaybe(data);
        if (ascii != null) {
            TextView text2 = (TextView) view.findViewById(android.R.id.text2);
            text2.setText(ascii);
        }
        z[0]=0;
        count=0;
        while(count< data.length) {

            Log.i(TAG, "Data : " +data[count]);

            //Search for '!' in the data

            if(data[count]=='!'){
                SensorNotConnected.setText("Sensor Not Connected");
            }
            if(data[count]=='/') {
                Log.i(TAG, "i found /");
            }
            else {
                Log.i(TAG,"RealData : " + data[count]);
                accelorSeries.appendData(new DataPoint(i++, data[count]), true, 10000);
//                if(j>1)
//                    filterSeries.appendData(new DataPoint(j++, z[j-2]+ temp *(data[count]-z[j-2])),true, 10000);

                newAccelorSeries.appendData(new DataPoint(k++, data[count] - preData), true, 10000);

                if(abs(data[count]-preData)> 5)
                    peak++;

                percentage = (20-peak)*100/20;

                noOfLevel.setText("Peaks :" +peak);
                perOfDrowiness.setText("Drowiness(%):" + percentage);

                preData= data[count];
//                z[j-2]= preData;
            }

            //filterSeries.appendData(new DataPoint(j++, z[j-1]+ temp *(data[count]-z[j-1])),true, 10000);


//            newAccelorSeries.appendData(new DataPoint(k++, data[count] - preData), true, 10000);
           /* if(abs(data[count]-preData)> 5)
                peak++;

            percentage = (20-peak)*100/20;

            noOfLevel.setText("Peaks :" +peak);
            perOfDrowiness.setText("Drowiness(%):" + percentage); */

//            preData= data[];
//            z[j-1]= preData;
            count++;

//            Log.i(TAG, "Data : " +data[count]);
        }
//        peak=0;



        dataLayout.addView(
                view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        if(data==null) {
            Log.i(TAG,"Receiving null data");
            return;
        }

        Log.i(TAG,"Data received: "+data.length);

    }

    Bitmap maskingImage;

    public static Bitmap scaleBitmap(Bitmap bitmap, int wantedWidth, int wantedHeight) {
        Bitmap output = Bitmap.createBitmap(wantedWidth, wantedHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Matrix m = new Matrix();
        m.setScale((float) wantedWidth / bitmap.getWidth(), (float) wantedHeight / bitmap.getHeight());
        canvas.drawBitmap(bitmap, m, new Paint());

        return output;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BitmapFactory.Options options=new BitmapFactory.Options();
        options.outWidth=371;
        options.outHeight=698;


        maskingImage = BitmapFactory.decodeResource(getResources(), R.raw.foot_image,options);

        maskingImage=scaleBitmap(maskingImage,371,698);


        textViewConnectionStatus = (TextView) findViewById(R.id.textViewConnectionStatus);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

//        getSupportActionBar().setTitle(mDeviceName);
//        getSupportActionBar()m.setDisplayHomeAsUpEnabled(true);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);





        dataLayout = (LinearLayout) findViewById(R.id.dataLayout);

        noOfLevel= (TextView) findViewById(R.id.level);
        perOfDrowiness= (TextView) findViewById(R.id.percent);

        SensorNotConnected= (TextView) findViewById(R.id.statusConnected);

        clearButton = (Button) findViewById(R.id.clearData);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataLayout.removeAllViews();
            }
        });

        //Graph plotting
        mainGraph = (GraphView) findViewById(R.id.mainGraph);

        mainGraph.getViewport().setScrollable(true);
        mainGraph.getViewport().setScalable(true);
        mainGraph.getViewport().setMaxX(100);




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"in on pause");

        unregisterReceiver(rfduinoReceiver);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

//    private void upgradeState(int newState) {
//        if (newState > state) {
//            updateConnectionState(newState);
//        }
//    }
//
//    private void downgradeState(int newState) {
//        if (newState < state) {
//            updateState(newState);
//        }
//    }


    @Override
    protected void onStart() {
        super.onStart();

//        registerReceiver(bluetoothStateReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        registerReceiver(rfduinoReceiver, RFduinoService.getIntentFilter());

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                mBluetoothLeService.disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Code to manage Service lifecycle.
//    private final ServiceConnection mServiceConnection = new ServiceConnection() {
//
//        @Override
//        public void onServiceConnected(ComponentName componentName, IBinder service) {
//            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
//            if (!mBluetoothLeService.initialize()) {
//                Log.e(TAG, "Unable to initialize Bluetooth");
//                finish();
//            }
//            // Automatically connects to the device upon successful start-up initialization.
//            mBluetoothLeService.connect(mDeviceAddress);
//        }
//
//        @Override
//        public void onServiceDisconnected(ComponentName componentName) {
//            mBluetoothLeService = null;
//        }
//    };

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (mBluetoothLeService.initialize()) {
                if (mBluetoothLeService.connect(mDeviceAddress)){
//                    upgradeState(STATE_CONNECTING);
                    updateConnectionState(R.string.connected);
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
//            downgradeState(STATE_DISCONNECTED);
            updateConnectionState(R.string.disconnected);
        }
    };

    private void handleCharacteristics(List<BluetoothGattService> gattServices) {
        if (gattServices == null)
            return;
        if (gattServices == null) return;
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
            }
            mGattCharacteristics.add(charas);
        }
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewConnectionStatus.setText(resourceId);
            }
        });
    }


    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
//    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            // TODO: When data is received
//            Log.i(TAG,"yoobru");
//
//            final String action = intent.getAction();
//            Log.i(TAG,action);
//            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
//                mConnected = true;
//                updateConnectionState(R.string.connected);
//                //noinspection RestrictedApi
//                invalidateOptionsMenu();
//            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
//                mConnected = false;
//                updateConnectionState(R.string.disconnected);
//                //noinspection RestrictedApi
//                invalidateOptionsMenu();
//            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
//                // Show all the supported services and characteristics on the user interface.
//                handleCharacteristics(mBluetoothLeService.getSupportedGattServices());
//                // Subscribe to characteristics to receive data
//                subscribe();
//            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
//                Log.i(TAG,"DATARECEVED");
//                addData(intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA));
//
//            }
//        }
//    };

    private final BroadcastReceiver rfduinoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG,"receiver");
            final String action = intent.getAction();
            Log.i(TAG,"Action"+ action);
            if (RFduinoService.ACTION_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                //noinspection RestrictedApi
                invalidateOptionsMenu();
               // upgradeState(STATE_CONNECTED);
            } else if (RFduinoService.ACTION_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                //noinspection RestrictedApi
                invalidateOptionsMenu();
                //downgradeState(STATE_DISCONNECTED);
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                Log.i(TAG,"got equal");
                dataReceived(intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };



    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(rfduinoReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }

        accelorSeries = new LineGraphSeries<>();
        newAccelorSeries= new LineGraphSeries<>();
        filterSeries= new LineGraphSeries<>();

        accelorSeries.setColor(Color.RED);
        newAccelorSeries.setColor(Color.BLUE);
        filterSeries.setColor(Color.CYAN);

        mainGraph.addSeries(accelorSeries);
        mainGraph.addSeries(newAccelorSeries);
        mainGraph.addSeries(filterSeries);
    }


    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
    int [] values=new int[6];

    AsyncTask myTask;

}