package com.example.apple.titan.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import com.example.apple.titan.R;

public class main_page extends AppCompatActivity implements View.OnClickListener{

    private final AppCompatActivity activity = main_page.this;
    private NestedScrollView nestedScrollView;

    private AppCompatButton appCompatButtonExit;
    private  AppCompatButton appCompatButtonEOGConnectivity;
    private  AppCompatButton appCompatButtonStartMonitoring;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        getSupportActionBar().hide();

        initViews();
        initListeners();
    }

    private void initViews() {

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        appCompatButtonExit = (AppCompatButton) findViewById(R.id.Exit);
        appCompatButtonEOGConnectivity = (AppCompatButton) findViewById(R.id.EOGconnectivity);
        appCompatButtonStartMonitoring = (AppCompatButton) findViewById(R.id.StartMonitoring);
    }

    private void initListeners()
    {
        appCompatButtonExit.setOnClickListener(this);
        appCompatButtonEOGConnectivity.setOnClickListener(this);
        appCompatButtonStartMonitoring.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        switch (view.getId())
        {
            case R.id.Exit:
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                break;

            case R.id.EOGconnectivity :
                Intent EOGconnectivity = new Intent(main_page.this,Splash.class);
                startActivity(EOGconnectivity);
                break;

            case R.id.StartMonitoring :
                final Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(MainActivity.EXTRAS_DEVICE_NAME, a);
                intent.putExtra(MainActivity.EXTRAS_DEVICE_ADDRESS, b);
                startActivity(intent);
                break;

        }
    }

    public static String a;
    public static String b;
    public  static void  onStartMain(String a, String b){
        main_page.a= a;
        main_page.b= b;

    }
}
