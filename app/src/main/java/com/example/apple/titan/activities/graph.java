package com.example.apple.titan.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.apple.titan.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import static com.example.apple.titan.R.id.dataLayout;

public class graph extends AppCompatActivity {

    private GraphView mainGraph, filterGraph;
    private LineGraphSeries<DataPoint> tempSeries, proxSeries , accelorSeries, lightSeries;
    private int i,j;
    private Button previous;



        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph2);

        previous = (Button) findViewById(R.id.previous);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(graph.this, MainActivity.class);
                startActivity(intent);
            }
        });

        mainGraph = (GraphView) findViewById(R.id.mainGraph);
        filterGraph = (GraphView) findViewById(R.id.filterGraph);

        mainGraph.getViewport().setScrollable(true);
        mainGraph.getViewport().setScalable(true);
        mainGraph.getViewport().setMaxX(100);

        filterGraph.getViewport().setScrollable(true);
        filterGraph.getViewport().setScalable(true);
        filterGraph.getViewport().setMaxX(100);

//        Bundle b=this.getIntent().getExtras();
//        String[] array=b.getStringArray(key);


    }

//    private void addData(byte[] data) {
//        View view = getLayoutInflater().inflate(android.R.layout.simple_list_item_2, dataLayout, false);
//
//        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
//        text1.setText(HexAsciiHelper.bytesToHex(data));
//
//        String ascii = HexAsciiHelper.bytesToAsciiMaybe(data);
//        if (ascii != null) {
//            TextView text2 = (TextView) view.findViewById(android.R.id.text2);
//            text2.setText(ascii);
//        }
//
//    }

//    @Override
//    protected  void onResume() {
//        super.onResume();
//        mainSeries = new LineGraphSeries<>();
//        filterSeries = new LineGraphSeries<>();
//
//        mainGraph.addSeries(tempSeries);
//        filterGraph.addSeries(proxSeries);
//
//
//        i=j=0;
//    }
}
